package maze;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class MazeTest {
    @Test
    void should_initialize_one_cell_maze() {
        String expectedRender1x1 = """
                ####
                #  #
                ####""";
        assertEquals(expectedRender1x1, Maze.of(1,1).render());
    }

    @Test
    void should_initialize_maze_with_size_of_empty_cell() {
        String expectedRender2x2 = """
                #######
                #  #  #
                #######
                #  #  #
                #######""";
        assertEquals(expectedRender2x2, Maze.of(2,2).render());
    }

    @Test
    void should_create_one_entry_and_one_exit() {
        String expectedRender1x1 = """
            ####
               #
            #  #""";
        String expectedRender2x2 = """
            ####  #
            #  #  #
            #######
               #  #
            #######""";

        assertEquals(expectedRender1x1, Maze.of(1, 1).generateEntryExit(fixDispatcher).render());
        assertEquals(expectedRender2x2, Maze.of(2, 2).generateEntryExit(fixDispatcher).render());
    }
}
