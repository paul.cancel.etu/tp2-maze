package maze;

public class Maze {

    private final static String CASE_VIDE = "  ";
    private final static String MUR = "#";

    private CellState[][] maze;

    private int[] entry;
    private int[] exit;

    public Maze(int nbRows, int nbCols) {
        this.maze = new CellState[calcSize(nbRows)][calcSize(nbCols)];
        for (int row = 0; row < maze.length; row++) {
            for (int col = 0; col < maze[1].length; col++) {
                if (col % 2 == 0) {
                    this.maze[row][col] = CellState.WALL;
                } else if (row % 2 == 1 && col % 2 == 1) {
                    this.maze[row][col] = CellState.DOUBLE_EMPTY;
                } else {
                    this.maze[row][col] = CellState.DOUBLE_WALL;
                }
            }
        }
    }

    
    private int calcSize(int nbRows) {
        return nbRows * 2 + 1;
    }

    public static Maze of(int i, int j) {
        return new Maze(i, j);
    }

    public String render() {
        StringBuilder rs = new StringBuilder();

        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[1].length; j++) {
                rs.append(maze[i][j].toString());
            }

            rs.append("\n");
        }
        
        return rs.substring(0, rs.length() - 1);
    }


    public void generateEntryExit(Dispatcher fixDispatcher){

    }


    public int[] getEntry() {
        return entry;
    }

    public void setEntry(int[] entry) {
        this.entry = entry;
    }

    public int[] getExit() {
        return exit;
    }

    public void setExit(int[] exit) {
        this.exit = exit;
    }
    
    public CellState[][] getMaze(){
        return this.maze;
    }
}
