package maze;

public enum CellState {
    WALL("#"),
    EMPTY(" "),
    DOUBLE_WALL("##"),
    DOUBLE_EMPTY("  ");

    private String str;

    private CellState(String str) {
        this.str = str;
    }

    public String toString(){
        return this.str;
    }
}
