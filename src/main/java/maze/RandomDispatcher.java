package maze;

import java.util.ArrayList;
import java.util.Collections;

public class RandomDispatcher implements Dispatcher{

    @Override
    public void setEntry(Maze maze) {
        ArrayList<int[]> possibleEntries = new ArrayList<>();

        for(int i = 0; i<maze.getMaze().length; i+=2){
            possibleEntries.add(new int[]{i, 0});
            possibleEntries.add(new int[]{i, maze.getMaze().length-1});
        }

        for(int i = 0 ; i<maze.getMaze()[0].length; i++){
            if(){
                possibleEntries.add(new int[]{0, i});
                possibleEntries.add(new int[]{maze.getMaze().length-1, i});
            }
        }

        Collections.shuffle(possibleEntries);
        maze.setEntry(possibleEntries.get(0));
    }

    @Override
    public void setExit(Maze maze) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'setExit'");
    }
    
}
